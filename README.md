##Тестовое задание для Genesys


**Запуск программы**

Необходимо запустить файл [main.py](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/src/main.py) в пакете src для начала работы с программой

**Структура программы**

В файле [person.py](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/src/person.py) содержится шаблон записи, содержащий имя человека, номер и дату рождения.

В файле [service.py](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/src/service.py) находится логика программы, отвечает за добавление, удаление людей из записной
книжки, поиск по имени, также за запись и чтение из файла [notebook](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/src/notebook), в котором хранятся записи.

В файле [main.py](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/src/main.py) находится логика, отвечающая за отображение информации, и точка входа в приложение.

**Функции программы**

В программе реализованы функции:

* **show** - выводит всех, кто содержится в записной книжке;
* **find** - выводит записи, в имени которых содержится введенная подстрока;
* **add** - добавляет запись в записную книжку;
* **remove** - удаляет запись из книжки по имени;
* **reminder** - выводит людей, празднующих день рождения сегодня;
* **exit** - выход из приложения.

*Имя записи должно быть уникально. Может содержать только буквы и пробелы. Телефон может содержать только цифры.
Введенные день, месяц и год должны быть верной датой.*

**Чек-лист для проверки**

Лежит здесь [checklist](https://bitbucket.org/dmitryistomin/genesystesttask/src/master/testtask_checklist.doc)
