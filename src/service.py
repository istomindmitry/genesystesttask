import datetime

from src.person import Person


def add(name, phone, birthday):
    person = Person(name, phone, birthday)
    notebook.append(person)
    write()


def find(name):
    result = []
    for i in notebook:
        if i.name.lower().find(name) != -1:
            result.append(i)
    return result


def remove(name):
    for i in notebook:
        if i.name == name:
            notebook.remove(i)
            write()
            return True
    return False


def edit(name, new_phone):
    person = find(name)
    person.phone = new_phone
    write()


def write():
    with open("notebook", 'w') as f:
        for i in notebook:
            f.write(i.name + "!" + i.phone + "!" + str(i.birthday.year) + "." + str(i.birthday.month) + "." + str(
                i.birthday.day) + "\n")


def read():
    n = []
    with open("notebook", 'r') as f:
        for line in f:
            person = line.split("!")
            date = person[2].split(".")
            n.append(Person(person[0], person[1], datetime.date(int(date[0]), int(date[1]), int(date[2]))))
    return n


def get_notebook():
    return notebook


def person_exists(name):
    name = name.lower()
    for i in notebook:
        if i.name.lower() == name:
            return True
    return False


def get_celebrators(day, month):
    result = []
    for i in notebook:
        if i.birthday.day == day and i.birthday.month == month:
            result.append(i)
    return result


notebook = read()
