class Person:
    def __init__(self, name, phone, birthday):
        self.name = name
        self.phone = phone
        self.birthday = birthday

    def __str__(self):
        return self.name + " " + self.phone + " " + str(self.birthday.day) + "/" + str(self.birthday.month) + "/" + str(
            self.birthday.year)
