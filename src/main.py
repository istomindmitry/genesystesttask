#! /usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from src.service import get_notebook, find, get_celebrators, person_exists, add, remove


def main():
    reminder()
    while True:
        print "Выберите функцию: add, show, find, remove, reminder, exit"
        line = raw_input()
        if line == "find":
            find_person()
        elif line == "exit":
            break
        elif line == "show":
            show_all()
        elif line == "add":
            add_person()
        elif line == "remove":
            remove_person()
        elif line == "reminder":
            reminder()


def find_person():
    print "Введите полностью или часть имени человека"
    found = find(raw_input().lower())
    if found:
        print_notebook(found)
    else:
        print "Поиск не дал результатов"


def show_all():
    found = get_notebook()
    print_notebook(found)


def print_notebook(notebook):
    for i in notebook:
        print i


def reminder():
    today = datetime.datetime.now()
    found = get_celebrators(today.day, today.month)
    if found:
        print "Сегодня празднуют день рождения:"
        print_notebook(found)
    else:
        print "Сегодня никто не празднует день рождения"


def add_person():
    print "Введите данные пользователя в формате ИМЯ.ТЕЛЕФОН.ДЕНЬ.МЕСЯЦ.ГОД"

    in_string = raw_input().split(".")
    name = in_string[0]
    if person_exists(name):
        print "Пользователь уже существует"
        return
    if not name.replace(" ", "").isalpha():
        print "Имя может содержать только буквы и пробелы"
        return
    phone = in_string[1]
    if not phone.isdigit():
        print "Телефон может содержать только цифры"
    try:
        day = int(in_string[2])
        month = int(in_string[3])
        year = int(in_string[4])
        birthday = datetime.datetime(year, month, day)
    except ValueError:
        print "Дата введена неверно"
        return
    add(name, phone, birthday)
    print "Запись добавлена успешно"


def remove_person():
    print "Введите имя человека, которого хотите удалить:"
    name = raw_input()
    if remove(name):
        print "Удаление прошло успешно"
    else:
        print "Объект не был найден"


main()
